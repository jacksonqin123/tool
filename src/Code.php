<?php
// +----------------------------------------------------------------------
// | najing [ 通用后台管理系统 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://www.najingquan.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 救火队队长
// +----------------------------------------------------------------------

namespace  Najingquan\Tool;

/**
 * 生成编号工具类
 * @author 救火队队长
 */
class Code
{
    /**
     * 获取编号
     * @author 救火队队长
     */
    public function getSn($type = 0)
    {
        switch ($type) {
            case 1:         //订单编号
                $str = $type . substr(msectime() . rand(0, 9), 1);
                break;
            case 2:         //商品编号
                $str = 'G' . substr(msectime() . rand(0, 5), 1);
                break;
            default:        //默认编号
                $str = substr(msectime() . rand(0, 5), 1);
        }
        return $str;
    }
}
